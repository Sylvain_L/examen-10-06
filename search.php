<?php include_once 'header.php' ;

        if (!empty($_GET)) {

            $count_result = 0;
            $search_results = array();

            if (!empty($_GET['q'])) {

                $search = $_GET['q'];
                $query = $db->prepare('SELECT * FROM products WHERE name LIKE :search  OR description LIKE :search LIMIT 4');
                $query->bindValue('search', '%'.$search.'%');
                $query->execute();
                $search_results = $query->fetchAll();

                $query = $db->prepare('SELECT COUNT(*) as count_result FROM products WHERE name LIKE :search  OR description LIKE :search');
                $query->bindValue('search', '%'.$search.'%');
                $query->execute();
                $result = $query->fetch();
                $count_result = $result['count_result'];
            }

            if (!empty($_GET['price'])) {
                $prices = ($_GET['price']);
                debug($prices);

            }
            if (!empty($_GET['keywords'])) {
                $keywords = $_GET['keywords'];
                $query = $db->prepare('SELECT COUNT(*) as count_result FROM products WHERE name LIKE :keywords  OR description LIKE :keywords OR price ');
                $query->bindValue('search', '%'.$search.'%');
                $query->execute();
                $result = $query->fetch();
                $count_result = $result['count_result'];
            }
        }
?>
        <div class="row">
            <div class="col-lg-12">

                <h1 class="page-header">Search</h1>

                <form class="search form-inline" method="GET">
                    <div class="form-group">
                        <input type="text" id="keywords" name="keywords" class="form-control" placeholder="Keywords" value="">
                    </div>

                    <div class="form-group">
                        <select id="category" name="category" class="form-control">
                            <option value="">Category</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="price">Price</label>
                        0 € <input id="price" name="price" type="text" value="" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="[0,100]"/> 100 €
                    </div>

                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="picture" name="picture" value="1"> Picture
                        </label>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search
                        </button>
                    </div>
                </form>

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?= $count_result ?> search results</h1>
            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="pull-right">
                    <form class="form-inline" method="GET">
                        <div class="form-group">
                            <label for="sort">Sort by</label>
                            <select id="sort" name="sort" class="form-control">
                                <option value="name">Name</option>
                                <option value="price">Price</option>
                                <option value="rating">Rating</option>
                                <option value="reviews">Reviews</option>
                            </select>
                            <select id="direction" name="direction" class="form-control">
                                <option value="ASC">Ascending</option>
                                <option value="DESC">Descending</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default">
                                <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                            </button>
                        </div>
                    </form>
                </div>

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->

        <hr>

        <div class="row">
            <div class="col-lg-12">

                <?php
                    if(!empty($search_results)) {
                        foreach ($search_results as $key => $search_result) :
                ?>

                <div class="product col-lg-3 col-md-4 col-xs-6 thumb">
                    <div class="thumbnail">
                        <img src="<?= $search_result['picture'] ?>" alt="">
                        <div class="caption">
                            <h4 class="pull-right"><?= $search_result['price'] ?> €</h4>
                            <h4><a href="product.php?id=<?= $search_result['id'] ?>"><?= $search_result['name'] ?></a>
                            </h4>
                            <p><?= nl2br(cutString($search_result['description'], 100)) ?> ?></p>
                        </div>
                        <div class="ratings">
                            <p class="pull-right"><?= rand(8, 20) ?> reviews</p>
                            <p>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            </p>
                        </div>
                        <div class="btns clearfix">
                            <a class="btn btn-info pull-left" href=""><span class="glyphicon glyphicon-eye-open"></span> View</a>
                            <a class="btn btn-primary pull-right" href=""><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                        </div>
                    </div><!-- /.thumbnail -->
                </div><!-- /.product -->
                <?php endforeach; 
                } else {
                    echo '<div class="alert alert-danger">AUCUN RESULTAT</div>';
                    }; ?>
            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->

    <?php include_once 'footer.php' ?>